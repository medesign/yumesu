<!DOCTYPE html>
<html lang="en">

<head>
   <title>@yield('judul')</title>
   <!-- HTML5 Shim and Respond.js IE9 support of HTML5 elements and media queries -->
   <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
   <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

   <!-- Meta -->
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
   <meta http-equiv="X-UA-Compatible" content="IE=edge" />
   <meta name="description" content="Quantum Able Bootstrap 4 Admin Dashboard Template by codedthemes">
   <meta name="keywords" content="appestia, Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
   <meta name="author" content="codedthemes">

   <!-- Favicon icon -->
   <link rel="shortcut icon" href="{{asset('admin/assets/images/favicon.png')}}" type="image/x-icon">
   <link rel="icon" href="{{asset('admin/assets/images/favicon.ico')}}" type="image/x-icon">

   <!-- Google font-->
   <link href="https://fonts.googleapis.com/css?family=Ubuntu:400,500,700" rel="stylesheet">

   <!-- themify -->
   <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/icon/themify-icons/themify-icons.css')}}">

   <!-- iconfont -->
   <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/icon/icofont/css/icofont.css')}}">

   <!-- simple line icon -->
   <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/icon/simple-line-icons/css/simple-line-icons.css')}}">

   <!-- Required Fremwork -->
   <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/plugins/bootstrap/css/bootstrap.min.css')}}">

   <!-- Style.css -->
   <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/css/main.css')}}">

   <!-- Responsive.css-->
   <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/css/responsive.css')}}">



</head>

<body class="sidebar-mini fixed">
   <div class="wrapper">
      @include('sweetalert::alert')
      <div class="loader-bg">
         <div class="loader-bar">
         </div>
      </div>
      <!-- Navbar-->
      @include('partial.nav')

      <!-- Side-Nav-->
      @include('partial.side')
      <!-- Sidebar chat end-->
      <div class="content-wrapper">
         <!-- Container-fluid starts -->
         <div class="container-fluid">

            <!-- Header Starts -->
            <section class="content-header">
               <div class="row">
                  <div class="col-sm-12 p-0">
                     <div class="main-header">
                        <h4>@yield('judul')</h4>
                        <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                           <li class="breadcrumb-item">
                              <a href="index.html">
                                 <i class="icofont icofont-home"></i>
                              </a>
                           </li>
                           <li class="breadcrumb-item"><a href="#">@yield('judul')</a>
                           </li>
                           <li class="breadcrumb-item"><a href="basic-table.html">@yield('header')</a>
                           </li>
                        </ol>
                     </div>
                  </div>
               </div>
            </section>
            <!-- Header end -->
            <section class="content">
               <div class="row">
                  <div class="col-md-12">
                     <div class="card">
                        <div class="card-block">
                           <div class="md-card-block mx-3">
                              @yield('content1')
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
         </div>
         <!-- Container-fluid ends -->
      </div>
   </div>

   <!-- Warning Section Starts -->
   <!-- Older IE warning message -->
   <!--[if lt IE 9]>
<div class="ie-warning">
    <h1>Warning!!</h1>
    <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>
    <div class="iew-container">
        <ul class="iew-download">
            <li>
                <a href="http://www.google.com/chrome/">
                    <img src="assets/images/browser/chrome.png" alt="Chrome">
                    <div>Chrome</div>
                </a>
            </li>
            <li>
                <a href="https://www.mozilla.org/en-US/firefox/new/">
                    <img src="assets/images/browser/firefox.png" alt="Firefox">
                    <div>Firefox</div>
                </a>
            </li>
            <li>
                <a href="http://www.opera.com">
                    <img src="assets/images/browser/opera.png" alt="Opera">
                    <div>Opera</div>
                </a>
            </li>
            <li>
                <a href="https://www.apple.com/safari/">
                    <img src="assets/images/browser/safari.png" alt="Safari">
                    <div>Safari</div>
                </a>
            </li>
            <li>
                <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                    <img src="assets/images/browser/ie.png" alt="">
                    <div>IE (9 & above)</div>
                </a>
            </li>
        </ul>
    </div>
    <p>Sorry for the inconvenience!</p>
</div>
<![endif]-->
   <!-- Warning Section Ends -->


   <!-- Required Jqurey -->
   <script src="{{asset('admin/assets/plugins/jquery/dist/jquery.min.js')}}"></script>
   <script src="{{asset('admin/assets/plugins/jquery-ui/jquery-ui.min.js')}}"></script>
   <script src="{{asset('admin/assets/plugins/tether/dist/js/tether.min.js')}}"></script>

   <!-- Required Fremwork -->
   <script src="{{asset('admin/assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>

   <!-- waves effects.js -->
   <script src="{{asset('admin/assets/plugins/Waves/waves.min.js')}}"></script>

   <!-- Scrollbar JS-->
   <script src="{{asset('admin/assets/plugins/jquery-slimscroll/jquery.slimscroll.js')}}"></script>
   <script src="{{asset('admin/assets/plugins/jquery.nicescroll/jquery.nicescroll.min.js')}}"></script>

   <!--classic JS-->
   <script src="{{asset('admin/assets/plugins/classie/classie.js')}}"></script>

   <!-- notification -->
   <script src="{{asset('admin/assets/plugins/notification/js/bootstrap-growl.min.js')}}"></script>

   <!-- custom js -->
   <script type="text/javascript" src="{{asset('admin/assets/js/main.min.js')}}"></script>
   <script type="text/javascript" src="{{asset('admin/assets/pages/elements.js')}}"></script>
   <script src="{{asset('admin/assets/js/menu.min.js')}}"></script>
</body>

</html>
