<!DOCTYPE html>
<html lang="en">
<head>
	<title>Quantum Able Bootstrap 4 Admin Dashboard Template</title>

	 <!-- HTML5 Shim and Respond.js IE9 support of HTML5 elements and media queries -->
   	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  		<!--[if lt IE 9]>
  			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  			<![endif]-->

	<!-- Meta -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<meta name="description" content="codedthemes">
	<meta name="keywords"
	content=", Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
	<meta name="author" content="codedthemes">

	<!-- Favicon icon -->
	<link rel="shortcut icon" href="{{asset('admin/assets/images/favicon.png')}}" type="image/x-icon">
	<link rel="icon" href="{{asset('admin/assets/images/favicon.ico')}}" type="image/x-icon">

	<!-- Google font-->
   <link href="https://fonts.googleapis.com/css?family=Ubuntu:400,500,700" rel="stylesheet">

	<!--ico Fonts-->
	<link rel="stylesheet" type="text/css" href="{{asset('admin/assets/icon/icofont/css/icofont.css')}}">

    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/plugins/bootstrap/css/bootstrap.min.css')}}">

	<!-- Style.css -->
	<link rel="stylesheet" type="text/css" href="{{asset('admin/assets/css/main.css')}}">

	<!-- Responsive.css-->
	<link rel="stylesheet" type="text/css" href="{{asset('admin/assets/css/responsive.css')}}">



</head>
<body>
	<section class="login common-img-bg">
		<!-- Container-fluid starts -->
		<div class="container-fluid";>
			<div class="row">
					<div class="col-sm-12">
						<div class="login-card card-block bg-white">
							<form class="md-float-material" action="{{ route('register') }}" method="POST" enctype="multipart/form-data">
								@csrf
								<!-- <div class="text-center">
									<img src="assets/images/logo-black.png" alt="logo">
								</div> -->
								<h3 class="text-center txt-primary">Create an account </h3>
										<div class="md-input-wrapper">
											<input type="text" name="name" class="md-form-control" required="">
											<label>Name</label>
										</div>
									@error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                	@enderror
								<div class="md-input-wrapper">
									<input type="email" name="email" class="md-form-control" required="required">
									<label>Email</label>
								</div>
									@error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                	@enderror
								<div class="md-input-wrapper">
									<input type="password" name="password" class="md-form-control" required="required">
									<label>Password</label>
								</div>
									@error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                	@enderror
								<div class="md-input-wrapper">
									<input type="password" name="password_confirmation" class="md-form-control" required="required">
									<label>Confirm Password</label>
								</div>
								<div class="md-input-wrapper">
									<input type="number" name="umur" class="md-form-control" required="required">
									<label>Umur</label>
								</div>
									@error('umur')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                	@enderror
								<div class="md-input-wrapper">
								<textarea name="alamat" class="md-form-control" placeholder="Alamat"></textarea>
								</div>
									@error('alamat')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                	@enderror
								<div class="md-input-wrapper">
									<textarea name="bio" class="md-form-control" placeholder="Bio"></textarea>
								</div>
									@error('bio')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                	@enderror
								<div class="md-input-wrapper">
									<p>Unggah Gambar Profil</p>
								<!-- <label>Unggah Gambar Profil</label> -->
									<input type="file" name="gambar_profil" class="md-form-control" required="required">
									
								</div>
									@error('gambar_profil')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                	@enderror
								<!-- <div class="rkmd-checkbox checkbox-rotate checkbox-ripple b-none m-b-20">
									<label class="input-checkbox checkbox-primary">
										<input type="checkbox" id="checkbox">
										<span class="checkbox"></span>
									</label>
									<div class="captions">Remember Me</div>
								</div> -->
								<div class="col-xs-10 offset-xs-1">
									<button type="submit" class="btn btn-primary btn-md btn-block waves-effect waves-light m-b-20">Sign up
									</button>
								</div>
								<div class="row">
									<div class="col-xs-12 text-center">
										<span class="text-muted">Already have an account?</span>
										<a href="/login" class="f-w-600 p-l-5"> Sign In Here</a>

									</div>
								</div>
							</form>
							<!-- end of form -->
						</div>
						<!-- end of login-card -->
					</div>
					<!-- end of col-sm-12 -->
				</div>
				<!-- end of row-->
			</div>
			<!-- end of container-fluid -->
	</section>

	<!-- Warning Section Starts -->
<!-- Older IE warning message -->
  <!--[if lt IE 9]>
      <div class="ie-warning">
          <h1>Warning!!</h1>
          <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>
          <div class="iew-container">
              <ul class="iew-download">
                  <li>
                      <a href="http://www.google.com/chrome/">
                          <img src="assets/images/browser/chrome.png" alt="Chrome">
                          <div>Chrome</div>
                      </a>
                  </li>
                  <li>
                      <a href="https://www.mozilla.org/en-US/firefox/new/">
                          <img src="assets/images/browser/firefox.png" alt="Firefox">
                          <div>Firefox</div>
                      </a>
                  </li>
                  <li>
                      <a href="http://www.opera.com">
                          <img src="assets/images/browser/opera.png" alt="Opera">
                          <div>Opera</div>
                      </a>
                  </li>
                  <li>
                      <a href="https://www.apple.com/safari/">
                          <img src="assets/images/browser/safari.png" alt="Safari">
                          <div>Safari</div>
                      </a>
                  </li>
                  <li>
                      <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                          <img src="assets/images/browser/ie.png" alt="">
                          <div>IE (9 & above)</div>
                      </a>
                  </li>
              </ul>
          </div>
          <p>Sorry for the inconvenience!</p>
      </div>
      <![endif]-->
      <!-- Warning Section Ends -->


   <!-- Required Jqurey -->
   <script src="{{asset('admin/assets/plugins/jquery/dist/jquery.min.js')}}"></script>
   <script src="{{asset('admin/assets/plugins/jquery-ui/jquery-ui.min.js')}}"></script>
   <script src="{{asset('admin/assets/plugins/tether/dist/js/tether.min.js')}}"></script>

   <!-- Required Fremwork -->
   <script src="{{asset('admin/assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>

   <!--text js-->
   <script type="text/javascript" src="{{asset('admin/assets/pages/elements.js')}}"></script>
</body>
</html>


