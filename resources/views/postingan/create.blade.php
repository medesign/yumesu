@extends('templates.master')

@section('judul')
    Postingan
@endsection

@section('content1')
<form action="/postingan" method="POST" enctype="multipart/form-data">
    @csrf

    <div class="form-group">
    <textarea name="isi" class="form-control" id="" cols="30" rows="10" placeholder="apa yang sedang kamu pikirkan..."></textarea>
    </div>
    @error('isi')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
 
    <div class="form-group">
        <input type="file" name="gambar" class="form-control">
    </div>

    <button type="submit" class="btn btn-primary">Posting</button>
</form>
@endsection