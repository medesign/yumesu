@extends('templates.master')

@section('judul')
    Komentar
@endsection

@section('content1')

<img style="width: 18rem;" src="{{asset('gambar/'.$posting->gambar)}}" alt="">
<h5>{{$posting->isi}}</h5>
<br>

<h4>Komentar :</h5><br>

@foreach ($posting->komentar as $item)
    
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <p class="card-text">{{$item->isi}}</p>
            </div>
        </div>
    </div>
</div>
@endforeach

<form action="/komentar" method="POST" enctype="multipart/form-data">
    @csrf

    <div class="form-group">
    <input type="hidden" name="postingan_id" value="{{$posting->id}}">
    <textarea name="isi" class="form-control" id="" cols="30" rows="10"></textarea>
    </div>
    @error('isi')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Komentar</button>
</form>

<br><a href="/postingan" class="btn btn-success">Kembali</a>
@endsection