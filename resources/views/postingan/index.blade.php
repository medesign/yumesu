@extends('templates.master')

@section('judul')
    Beranda
@endsection

@section('content1')
@auth
<a href="/postingan/create" class="btn btn-success btn-sm mb-3">Mulai Posting</a>
@endauth
<div class="row">
    @forelse ($postingan as $item)
        <div class="col-12">
            <div class="card">
                @if ($item->gambar===" ")
                    <p> </p>
                @else
                <img class="card-img-top" style="width: 18rem;" src="{{asset('gambar/'.$item->gambar)}}" alt="Card image cap">    
                @endif
                <div class="card-body">
                  <h5 class="card-text">{{$item->isi}}</h5>
                </div>
                @auth
                <form action="/postingan/{{$item->id}}" method="POST">
                @csrf
                <a href="/postingan/{{$item->id}}" class="btn">Like</a>
                <a href="/postingan/{{$item->id}}" class="btn">Komentar</a>
                <a href="/postingan/{{$item->id}}/edit" class="btn">Edit</a>
                @method('DELETE')
                @csrf
                <input type="submit" value="Delete" class="btn">
                </form>
                @endauth

              </div>
        </div>
    @empty
        <h1>Belum ada data postingan</h1>
    @endforelse
</div>
@endsection