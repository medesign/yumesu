<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/master', function () {
    return view('templates.master');
});

Route::get('/test-suhanda', function () {
    return "Halo ini suhanda";
});

Route::resource('postingan', 'PostinganController');
Route::resource('profile', 'ProfileController');
Route::resource('komentar', 'CastController');

Route::resource('/komentar', 'KomentarController')->only([
    'index', 'store'
]);

Auth::routes();
