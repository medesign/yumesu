<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class komentar extends Model
{
    protected $table = '_komentar';
    protected $fillable = ['user_id', 'postingan_id', 'isi'];

    public function postingan()
    {
        return $this->belongsTo('App\postingan');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
