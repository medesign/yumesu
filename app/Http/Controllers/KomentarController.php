<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\komentar;

class KomentarController extends Controller
{
    public function store(Request $request)
    {
        $request->validate([
            'isi' => 'required',
        ]);

        $komentar = new komentar();

        $komentar->user_id = Auth::id();
        $komentar->postingan_id = $request->postingan_id;
        $komentar->isi = $request->isi;

        $komentar->save();
        return redirect()->back();
    }
}
