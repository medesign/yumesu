<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\postingan;
use File;
use RealRashid\SweetAlert\Facades\Alert;

class PostinganController extends Controller
{
    // public function __construct()
    // {
    //     this->middleware('auth');
    // }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $postingan = postingan::all();
        return view('postingan.index', compact('postingan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('postingan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'isi' => 'required',
        ]);
        if ($request->has('gambar')) {

            $gambarName = time() . '.' . $request->gambar->extension();

            $request->gambar->move(public_path('gambar'), $gambarName);

            $posting = new postingan();

            $posting->isi = $request->isi;
            $posting->gambar = $gambarName;
        } else {
            $gambarName = " ";

            $posting = new postingan();

            $posting->isi = $request->isi;
            $posting->gambar = $gambarName;
        }

        $posting->save();

        Alert::success('Berhasil', 'Postinganmu Sudah Berhasil');

        return redirect('/postingan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $posting = postingan::findOrFail($id);
        return view('postingan.show', compact('posting'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $posting = postingan::findOrFail($id);
        return view('postingan.edit', compact('posting'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'isi' => 'required',
        ]);

        $posting = postingan::find($id);
        if ($request->has('gambar')) {
            $gambarName = time() . '.' . $request->gambar->extension();

            $request->gambar->move(public_path('gambar'), $gambarName);

            $posting->isi = $request->isi;
            $posting->gambar = $gambarName;
        } else {
            $posting->isi = $request->isi;
        }

        $posting->update();

        Alert::success('Edit Berhasil', 'Postinganmu Sudah Di Edit');

        return redirect('/postingan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $posting = postingan::find($id);

        $path = "gambar/";
        File::delete($path . $posting->gambar);

        $posting->delete();

        Alert::success('Terhapus', 'Postinganmu Sudah Di Hapus');

        return redirect('/postingan');
    }
}
