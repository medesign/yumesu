<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class postingan extends Model
{
    protected $table = 'postingan';
    protected $fillable = ['isi', 'gambar'];

    public function komentar()
    {
        return $this->hasMany('App\komentar');
    }
}
